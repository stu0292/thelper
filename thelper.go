// Package thelper: Simple helper functions for go tests.
package thelper

import (
	"path/filepath"
	te "testing"

	"fmt"
	"reflect"
	"runtime"

	"bufio"
	"bytes"
	"io"
	"io/ioutil"
	"os"
	"strings"
)

// Tdp (test data path) joins the filename to the test data path
func Tdp(name string) string {

	return filepath.Join("testdata", name) // relative path
}

// ReaderToFile creates a new file and copies the reader to its contents, closes file
func ReaderToFile(filename string, contents io.Reader, forceOverwrite bool) (written int64, err error) {

	// if we're not forcing overwrite, need to check if the file already exists
	if !forceOverwrite && FileExists(filename) {
		return 0, fmt.Errorf("Contents not written, file %s already exists", filename)
	}

	// create output file
	file, err := os.Create(filename)
	defer file.Close()
	if err != nil {
		return 0, fmt.Errorf("Error creating output file %s: %s", filename, err)
	}

	// copy contents to file
	written, err = io.Copy(file, contents)
	if err != nil {
		return 0, fmt.Errorf("Error writing file %s: %s", filename, err)
	}

	return
}

// FileExists returns true if a file exists
func FileExists(filepath string) bool {
	if _, err := os.Stat(filepath); os.IsNotExist(err) {
		return false
	} else {
		return true
	}
}

// Load loads the file from testdata directory. Source: https://medium.com/@povilasve/go-advanced-tips-tricks-a872503ac859
func Load(t *te.T, name string) string {

	bytes, err := ioutil.ReadFile(Tdp(name))
	FatalIf(t, err, err)

	// trim trailing newline before returning
	return strings.TrimSuffix(string(bytes), "\n")
}

// PrintRed prints with fmt.Printf, in red text.
// format and a are passed to fmt.Printf.
func PrintRed(format string, a ...interface{}) {

	// make the format red
	str := fmt.Sprintf("\033[31m%s\033[39m\n", format)

	// print the message, pass in the elements to Printf
	fmt.Printf(str, a...)
}

// Eq fails the test, if exp is not equal to act. Source: https://github.com/benbjohnson/testing.
// format and a are passed to fmt.Printf.
func Eq(tb te.TB, exp, act interface{}, format string, a ...interface{}) {

	if !reflect.DeepEqual(exp, act) {
		_, file, line, _ := runtime.Caller(1)
		PrintRed(format, a...)
		fmt.Printf("\033[31m%s:%d:\n\n\texp: %#v\n\n\tgot: %#v\033[39m\n\n", filepath.Base(file), line, exp, act)
		tb.FailNow()
	}
}

// EqReaderError: Readers provided different content.
type EqReaderError struct {
	aText, bText string
}

func NewEqReaderError(a, b *bufio.Scanner) error {
	return &EqReaderError{a.Text(), b.Text()}
}

func (e *EqReaderError) Error() string {
	return fmt.Sprintf("Readers provided different content.\na: '%v'\nb: '%v'", e.aText, e.bText)
}

// EqReader tests if two readers read equal content.
// Returns nil if two readers return same content.
// Returns EqReaderError if the readers are different.
// Returns any read errors.
// See also https://stackoverflow.com/a/30038571/6672339
func EqReader(a, b io.Reader) error {

	// make scanners
	sA := bufio.NewScanner(a)
	sB := bufio.NewScanner(b)

	// Scan through sA and sB
	for sA.Scan() {
		sB.Scan()

		// continue if equal content
		if bytes.Equal(sA.Bytes(), sB.Bytes()) {
			continue
		} else {
			return NewEqReaderError(sA, sB)
		}
	}

	// if sA.Scan above returns false, sB.Scan will not be called. Check sB.Scan also returns false
	if sB.Scan() {
		return NewEqReaderError(sA, sB)
	}

	// scanner errors
	if errA := sA.Err(); errA != nil {
		return errA
	}

	if errB := sB.Err(); errB != nil {
		return errB
	}

	// if no other returns, readers provide equal content
	return nil
}

// Open a file from the 'testdata' folder.
// Caller should .Close() the file after use.
// t.Fatal if the file open hits an error. To avoid this behaviour, use os.Open directly.
func Open(t *te.T, name string) *os.File {

	// open the file to return
	path := Tdp(name)
	file, err := os.Open(path)
	FatalIf(t, err, "Error opening file ", path, err)

	return file

}

// NewBuff is a convenience wrapper, returning a new bytes.Buffer.
// Can be used as an output writer that is then compared in EqFile.
func NewBuff() *bytes.Buffer {
	return bytes.NewBuffer(nil)
}

// EqFile loads a file from 'testdata' folder and compares with the provided io.Reader.
// Fails f the contents of act are not equal to the file contents.
// format and a are passed to fmt.Printf in the event the reader and file are not equal.
func EqFile(t *te.T, fileName string, reader io.Reader, format string, a ...interface{}) {

	// load exp file
	exp := Open(t, fileName)
	defer exp.Close()

	// compare actual to expected
	err := EqReader(exp, reader)
	if err != nil {
		// user's message
		PrintRed(format, a...)
		// EqReader error
		t.Log(err)
		t.FailNow()
	}
}

// IsNil fails the test based on whether (bIsNil && act == nil) is false.
// If bIsNil is true, act == nil should return true otherwise the test fails.
// If bIsNil is false, act == nil should return false otherwise the test fails.
// FIXME had issues using this in another package - might be better just to test if expression != nil { t.Log(); t.FailNow(); }
func IsNil(tb te.TB, bIsNil bool, act interface{}, format string, a ...interface{}) {
	// if bIsNil true, act should be nil. if bIsNil false act should be !nil. Otherwise test fails
	if (bIsNil && act != nil) || (!bIsNil && act == nil) {
		// test failed,
		PrintRed(format, a...)
		tb.FailNow()
	}
}

// FatalIf logs fatal if e is not nil.
func FatalIf(t *te.T, e error, a ...interface{}) {
	if e != nil {
		t.Fatal(e)
	}
}
