// Package thelper examples
package thelper

import (
	"fmt"
	"testing"
)

// This example is non-executable. For documentation purposes only.
func ExampleEqFile() {

	func(t *testing.T) {
		// write actual result to buffer for comparing to expected result file
		actual := NewBuff()
		fmt.Fprint(actual, `this value should be exactly the same as the content of /testdata/expected.txt`)

		// test if buffer equals file
		EqFile(t, "expected.txt", actual, "Actual result differed from expected")

	}(nil)
}
