// Package thelper tests
package thelper

import (
	"fmt"
	"strings"
	te "testing"
)

var sample = `Health
Happiness
Love
`
// TestReaderToFile
func TestReaderToFile(t *te.T) {
  // TODO
}

// TestEqFile
func TestEqFile(t *te.T) {

	// TODO: Negative testing. Partially covered by TestEqReader

	// write actual result to buffer for comparing to expected result file
	actual := NewBuff()
	fmt.Fprint(actual, sample)

	// test if buffer equals file
	EqFile(t, "expected.txt", actual, "Actual result differed from expected")
}

// TestPrintRed just displays output for eyeballing
func TestPrintRed(t *te.T) {

	// 1 arg
	PrintRed("Wow!")

	// 2 args
	PrintRed("IT'S SO PRETTY %v", ":O")

	// more args
	PrintRed("IT'S SO PRETTY %v %v %v", ":D", ":D", ":D")
}

func TestEqReader(t *te.T) {

	// compare io.Readers of strings
	cases := []struct {
		a, b string
		eq   bool
	}{
		{"", "", true},
		{"mellifluous", "mellifluous", true},
		{"mellifluous", "", false},
		{"", "mellifluous", false},
		{"a", "b", false},
		{`mutli
		line`, `mutli
		line`, true},
		{`different
		`, `mutli
		line`, false},
	}

	// test cases
	for i, c := range cases {

		// string readers
		rA := strings.NewReader(c.a)
		rB := strings.NewReader(c.b)

		// test ReaderEq
		err := EqReader(rA, rB)
		// log errors for eyeballing
		if err != nil {
			t.Log(c)
			t.Log(err)
		}
		// assert expectations
		if !c.eq && err == nil {
			t.Fatal("Error expected for different readers but none returned: ", i)
		}
		if c.eq && err != nil {
			t.Fatal("Expected readers to be equal but error was returned: ", i, err)
		}
	}
}

func TestFileExists(t *te.T) {

  // Test cases: 
  cases := map[string]bool{
    ".git": true,
    ".git/HEAD": true,
    "thelper.go": true,
    "banana": false,
  }

  for f, exists := range cases {

    Eq(t, exists, FileExists(f), "Does file %v exist?", f)
  }

}

func TestIsNil_isnil(t *te.T) {
  var e error

  // actual is nil
  IsNil(t, true, e, "e Is nil and true so shouldn't fail %v", e)
  // IsNil(t, false, e, "e Is nil but false so fails: %v", e)
}

func TestIsNil_isnil2(t *te.T) {
  var e error

  // actual is nil
  // IsNil(t, true, e, "e Is nil and true so shouldn't fail %v", e)
  IsNil(t, false, e, "e Is nil but false so fails: %v", e)
}

func TestIsNil_notnil(t *te.T) {
  s := "not nil"

  // actual is not nil
  IsNil(t, false, s, "s not nil but false so shouldn't fail: %v", s)
  // IsNil(t, true, s, "s not nil but isNil true so fails: %v", s)
}

func TestIsNil_notnil_2(t *te.T) {
  s := "not nil"

  // actual is not nil
  // IsNil(t, false, s, "s not nil but false so shouldn't fail: %v", s)
  IsNil(t, true, s, "s not nil but isNil true so fails: %v", s)
}
